const
  request = require('request'),
  path = require('path'),
  kill = require('tree-kill'),
  exec = require('child_process').exec,
  execSync = require('child_process').execSync,
  spawn = require('child_process').execFile;

const
  cwd = path.join(__dirname, 'player-app'),
  cmds = {
    installElectron: 'npm i electron',
    installElectronG: 'npm i -g electron',
    install: 'npm i',
    run: 'npm start'
  };

let playerAppProcess;

try {
  require.resolve('electron');
}
catch (err) {
  console.log('SEEMS LIKE electron NOT INSTALLED');
  console.log('>', cmds.installElectron);
  try {
    execSync(cmds.installElectron);
  }
  catch (err) {
    console.log('ERROR:', err);
    console.log('PLEASE CONSIDER INSTALLING electron PACKAGE MANUALLY BY RUNNING: npm i -g electron');
    process.exit(0);
  }
}

const
  installDependencies = callback => {
    console.log('PREINSTALLING PACKAGES');
    console.log('>', cmds.install);
    exec(
      cmds.install,
      {cwd},
      (err, stdOut) => {
        if(err) {
          console.log('ERROR:', err);
          process.exit(0);
        }
        
        console.log(stdOut);
        callback();
      });
  },
  runApp = (callback) => {
    console.log("STARTING PLAYER APP");
    console.log('>', cmds.run);
    const electronCli = path.join('node_modules', 'electron', 'cli.js');
    playerAppProcess = spawn('node', [electronCli, '.'], {cwd});
    playerAppProcess.stdout.on('data', (data) => {
      console.log('PLAYER SAYS >', data);
    });
    playerAppProcess.on('exit', () => {
      console.log('APPLICATION CLOSED');
      console.log('SPAWNING APP');
      runApp();
    });
    console.log('PLAYER APP STARTED');
  };

installDependencies(() => runApp());

let restartRequired = false;
setInterval(() => {
  if(restartRequired) {
    console.log('RESTART REQUIRED');

    installDependencies(() => {
      if(playerAppProcess && playerAppProcess.pid) {
        console.log('KILLING PROCESS');
        kill(playerAppProcess.pid);
        delete playerAppProcess;
      }
      restartRequired = false;
    });
    return;
  }
}, 300000);

const gracefullClose = (exitProcess = false) => {
  if(playerAppProcess && playerAppProcess.pid) {
    console.log('KILLING PROCESS');
    playerAppProcess.removeListener('exit');
    kill(playerAppProcess.pid);
    delete playerAppProcess;
  }
  
  if(exitProcess) {
    process.exit(0);
  }
};

process.on('exit', () => gracefullClose());
process.on('close', () => gracefullClose(true));