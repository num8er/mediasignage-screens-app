"use strict";

const path = require('path');

const config = {};
config.socketPort = ':80';
config.socketHostUrl = 'http://io.mediasignage.co';
config.socketRoute = '/';
config.updatesUrl = '';

config.database = {
  storagePath: path.join(__dirname, 'db'),
  structure: {
    assets: {
      entries: []
    },
    manifest: {},
    playlist: {
      updated_at: '',
      entries: {}
    },
    settings: {
      top: "0",
      left: "0",
      width: "100%",
      height: "100%",
      uid: "default"
    },
    stats: {}
  }
};

module.exports = config;