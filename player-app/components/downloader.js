const
  path = require('path'),
  fs = require('fs-extra'),
  EventEmitter = require('events'),
  request = require('request');

class Downloader extends EventEmitter {
  constructor(url, filename, directory) {
    super();
    this._url = url;
    this._dir = directory;
    this._file = path.join(directory, filename);
  }
  
  ensureDir(dir) {
    return new Promise((resolve, reject) => {
      fs.ensureDir(dir, error => {
        if(error) return reject(error);
        resolve(dir);
      });
    });
  }
  
  getFileStat(file) {
    return new Promise((resolve) => {
      fs.fstat(file, (error, stat) => {
        if(error) return resolve(null);
        resolve(stat);
      });
    });
  }

  getRemoteFileSize(uri) {
    return new Promise((resolve, reject) => {
      const options = {
        uri,
        method: 'HEAD',
        followAllRedirects: true
      };
      request(options, (error, response) => {
        if (response.statusCode >= 400) {
          return reject('Cannot get file size. Status code: ' + response.statusCode);
        }
        
        const contentLength = response.headers['content-length'] ? parseInt(response.headers['content-length']) : 0;
        resolve(contentLength);
      });
    });
  }
  
  createReadStreamFromUrl(uri, seek) {
    return request({
      uri,
      encoding: 'binary',
      method: 'GET',
      followAllRedirects: true,
      strictSSL: false,
      start: seek
    })
  }
  
  downloadFile(url, file, resume = false, seek = 0, bytesTotal) {
    const downloader = this;
    return new Promise((resolve, reject) => {
      let bytesDownloaded = 0;
      const readStream = this.createReadStreamFromUrl(url, seek);
      const writeStream = fs.createWriteStream(file);
      
      readStream.on('data', (buffer) => {
        bytesDownloaded += buffer.length;
        downloader.emit('stat', {bytesDownloaded, bytesTotal});
      });
      writeStream.on('finish', () => {
        downloader.emit('finish');
      });
      if(!resume) {
        readStream.pipe(writeStream);
      }
    });
  }
  
  download() {
    this
      .ensureDir(this._dir)
      .then(() => {
        return Promise.all([
          this.getFileStat(this._file),
          this.getRemoteFileSize(this._url)
        ])
      })
      .then(results => {
        const
          stat = results[0],
          remoteFileSize = results[1];
        
        if(stat === null) {
          return this.downloadFile(this._url, this._file, false, 0, remoteFileSize);
        }
        
        return (stat.size >= remoteFileSize) ?
                Promise.resolve(this._file)
                : this.downloadFile(this._url, this._file, true, stat.size, remoteFileSize);
      })
      .catch(error => {
        this.emit('error', error);
      });
  }
}

module.exports = Downloader;