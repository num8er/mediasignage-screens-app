const
  fs = require('fs'),
  path = require('path'),
  lowdb = require('lowdb'),
  readFile = fs.readFileSync,
  writeFile = fs.writeFileSync,
  existsSync = fs.existsSync,
  BaseAdapter = require('lowdb/adapters/Base');
  
class Adapter extends BaseAdapter {
  read() {
    if (existsSync(this.source)) {
      try {
        const data = readFile(this.source, 'utf-8').trim();
        return data ? this.deserialize(data) : this.defaultValue;
      } catch (e) {
        // don't trow error if json malformed
        if (! (e instanceof SyntaxError)) {
          throw e;
        }
      }
    }
  
    // initialize file with default value
    writeFile(this.source, this.serialize(this.defaultValue));
    return this.defaultValue;
  }
  
  write(data) {
    return writeFile(this.source, this.serialize(data))
  }
}

class Database {
  constructor(storagePath, dbStructure = {}) {
    this._storagePath = storagePath;
    fs.accessSync(
      this._storagePath,
      fs.constants.R_OK | fs.constants.W_OK | fs.constants.X_OK
    );
    this._dbStructure = dbStructure;
    this.initialize();
  }
  
  initialize() {
    this._storages = {};
    
    const extension = '.json';
    const entries = fs.readdirSync(this._storagePath);
    for(const entry of entries) {
      if(path.extname(entry) === extension) {
        const basename = path.basename(entry, extension);
        const adapter = new Adapter(
          path.join(this._storagePath, entry),
          {
            defaultValue: this._dbStructure[basename] ? this._dbStructure[basename] : {}
          }
        );
        this._storages[basename] = lowdb(adapter);
      }
    }
    
    for(const entry in this._dbStructure) {
      if(!this._storages[entry]) {
        const adapter = new Adapter(
          path.join(this._storagePath, entry+extension),
          {
            defaultValue: this._dbStructure[entry] ? this._dbStructure[entry] : {}
          }
        );
        this._storages[entry] = lowdb(adapter);
      }
    }
  }
  
  get(entry) {
    const entryParts = entry.split('.');
    if(entryParts.length < 1) return {};
    if(entryParts.length === 1) {
      return this._storages[entryParts[0]];
    }
    return this._storages[entryParts[0]].get(entryParts.slice(1).join('.'));
  }
  
  set(entry, data = {}) {
    const entryParts = entry.split('.');
    if(entryParts.length < 1) return {};
    if(entryParts.length === 1) {
      return this._storages[entryParts[0]].setState(data);
    }
    return this._storages[entryParts[0]].set(entryParts.slice(1).join('.'), data);
  }
}

module.exports = Database;