const Downloader = require('./components/downloader');

const downloader = new Downloader("https://s3.eu-central-1.amazonaws.com/mediasignage/library/mtIEE4eQCR00FGvJBMQwH2uggkDaebFZBO6nymeM.mp4", './', 'test.mp4');
downloader.on('stat', stat => {
  console.log({stat});
});
downloader.on('finish', () => {
  console.log('Finished');
});

downloader.download();