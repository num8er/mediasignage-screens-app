const
  _ = require('lodash'),
  fs = require('fs-extra'),
  path = require('path'),
  url = require('url'),
  download = require('download-file'),
  request = require('request'),
  async = require('async'),
  moment = require('moment'),
  config = require('./config'),
  package = require('./package.json');

const Database = require('./components/database-json');
const db = new Database(config.database.storagePath, config.database.structure);

const downloadingAssets = {};

const
  electron = require('electron'),
  app = electron.app,
  BrowserWindow = electron.BrowserWindow,
  ipcMain = electron.ipcMain,
  socketIO = require('socket.io-client');

let
  win,
  playerViewUrl = url.format({
    pathname: path.join(__dirname, 'views', 'player.html'),
    protocol: 'file:',
    slashes: true
  });

const
  createWindow = () => {
    win = new BrowserWindow({width: 1024, height: 800});
    win.loadURL(playerViewUrl);
    
    win.setFullScreen(true);
    
    win.setMenu(null);
    
    //win.webContents.openDevTools();
    
    win.on('closed', function() {
      win = null;
    });
  };

app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if(process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if(win === null) {
    createWindow();
  }
});

let
  socketUrl = [config.socketHostUrl, config.socketPort, config.socketRoute].join(''),
  socketHandshakeQuery = () => {
    return 'uid=' + db.get('settings.uid').value() + '&app=' + package.name + '&version=' + package.version;
  },
  socketConnect = () => {
    return socketIO(socketUrl + '?' + socketHandshakeQuery());
  };

let statsInterval;

const registerSocketHandlers = socket => {
  socket.on('connect', () => {
    console.log('CONNECTED');
    win.webContents.send('connect');
  });
  
  socket.on('message', (message) => {
    console.log('GOT MESSAGE:', message);
    
    win.webContents.send('message', message);
    switch(message) {
      case 'update' :
        buildPlaylist((err, playlist) => {
          if(err) return;
          
          if(playlist) {
            win.webContents.send('playlist', playlist);
          }
        });
        break;
    }
  });
  
  if(statsInterval) clearInterval(statsInterval);
  statsInterval = setInterval(() => {
    console.log('SENDING STATS');
    socket.emit('stats', db.get('stats').value());
  }, 60 * 60 * 1000);
  
  socket.on('stats:delivered', () => {
    console.log('STATS DELIVERED');
    db.set('stats', {}).write();
  })
};

let
  socket = socketConnect();
registerSocketHandlers(socket);

const sendPlayerSettings = () => {
  win.webContents.send('player-settings', {
    width: castToPixelOrPercentage(db.get('settings.width').value()),
    height: castToPixelOrPercentage(db.get('settings.height').value()),
    top: castToPixelOrPercentage(db.get('settings.top').value(), 0),
    left: castToPixelOrPercentage(db.get('settings.left').value(), 0)
  });
};
setInterval(() => sendPlayerSettings(), 1000);

setInterval(() => {
  buildPlaylist((err, playlist) => {
    if(err) return;
    
    if(playlist) {
      win.webContents.send('playlist', playlist);
    }
  });
}, 5 * 60 * 1000);

ipcMain
  .on('ready', () => {
    const playlist = db.get('playlist.entries').value();
    if(playlist) {
      win.webContents.send('playlist', playlist);
    }
    
    buildPlaylist((err, playlist) => {
      if(err) return;
      
      if(playlist) {
        win.webContents.send('playlist', playlist);
      }
    });
    
    win.webContents.send('set-uid', db.get('settings.uid').value());
  })
  .on('get-uid', () => {
    win.webContents.send('set-uid', db.get('settings.uid').value());
  })
  .on('set-uid', (event, uid) => {
    if(db.get('settings.uid').value() !== uid) {
      db.set('settings.uid', uid).write();
      
      socket.disconnect();
      socket = socketConnect();
      registerSocketHandlers(socket);
      
      buildPlaylist((err, playlist) => {
        if(err) return;
        
        if(playlist) {
          win.webContents.send('playlist', playlist);
        }
      });
      
      db.set('stats', {uid: db.get('settings.uid').value()}).write();
    }
  })
  .on('play-asset', (event, asset) => {
    const stats = db.get('stats').value();
    
    if(!stats.periodFrom) {
      stats.periodFrom = moment().format('YYYY-MM-DD HH:mm:ss');
    }
    stats.periodTo = moment().add(asset.duration, 'seconds').format('YYYY-MM-DD HH:mm:ss');
    
    if(!stats.played) stats.played = {};
    if(!stats.played[asset.assetId])
      stats.played[asset.assetId] = 0;
    stats.played[asset.assetId]++;
    
    db.set('stats', stats).write();
  });

const
  generatePlaylist = (manifest, callback) => {
    const playlist = {};
    let slides = _.get(manifest, 'schedules[0].playlists[0].slides');
    
    const
      lastUpdatedAt = db.get('playlist.updated_at').value(),
      lastPlaylist = db.get('playlist.entries').value();
    if(lastUpdatedAt === manifest.updated_at) {
      if(lastPlaylist) {
        return callback(null, lastPlaylist);
      }
    }
    
    console.log('GOT UPDATED MANIFEST. GENERATING PLAYLIST');
    
    if(slides) {
      slides = slides.sort((a, b) => a.slide_order - b.slide_order);
      
      let
        remainingDuration = 86400000,
        start = 0;
      
      while(remainingDuration > 0) {
        for(const slide of slides) {
          const id = _.get(slide, 'canvas.objects[0]').idAsset;
          const asset = db.get('assets.entries').find({id}).value();
          if(!asset) continue;
          
          playlist[start] = {};
          playlist[start].file = url.format({
            pathname: path.join(__dirname, asset.assetPath),
            protocol: 'file:',
            slashes: true
          });
          playlist[start].assetId = id;
          playlist[start].duration = parseInt(slide.duration / 1000);
          start += slide.duration;
          remainingDuration -= slide.duration;
          if(remainingDuration < 0) {
            break;
          }
        }
      }
      if(playlist) {
        db.set('playlist', {
          updated_at: manifest.updated_at,
          entries: playlist
        }).write();
        callback(null, playlist);
        return;
      }
    }
    callback();
  },
  castToPixelOrPercentage = (value, fallback) => {
    if(value === undefined) return fallback || 0;
    if(value === 0 || value === "0") return 0;
    if(value) {
      value = value.toString();
      if(value.indexOf('%') > 0) return value;
      if(value.indexOf('px') > 0) return value;
      return value + 'px';
    }
    return fallback || '100%';
  },
  buildPlaylist = callback => {
    const
      url = 'http://api.mediasignage.co/screens/' + db.get('settings.uid').value() + '/manifest';
    console.log('BUILDING PLAYLIST BY MANIFEST:', url);
    request(url, (err, response, body) => {
      if(err) return console.log('Err requesting manifest:', err);
      
      if(_.isString(body)) {
        try {
          body = JSON.parse(body);
        } catch(error) {
        }
      }
      db.set('manifest', body).write();
      
      const uid = body.uid;
      
      db.set('settings.width', castToPixelOrPercentage(body.width)).write();
      db.set('settings.height', castToPixelOrPercentage(body.height)).write();
      db.set('settings.top', castToPixelOrPercentage(body.top, 0)).write();
      db.set('settings.left', castToPixelOrPercentage(body.left, 0)).write();
      
      if(body.assets && body.assets.length > 0) {
        let
          assetsCount = body.assets.length,
          assetsDownloaded = 0;
        
        win.webContents.send('progress-start', {current: assetsDownloaded, count: assetsCount});
        
        async.eachSeries(
          body.assets.filter(asset => asset.type === 'video'),
          (asset, callback) => {
            if(uid !== db.get('settings.uid').value() && uid !== 'default') return callback('uid:changed');
            
            downloadAsset(asset, (error, asset) => {
              if(error) return callback(error);
              
              assetsDownloaded++;
              win.webContents.send('progress', {current: assetsDownloaded, count: assetsCount});
              
              callback(null, asset);
            });
          },
          err => {
            if(err) {
              if(err === 'uid:changed') {
                return console.log('Uid changed from:', uid, 'to:', db.get('settings.uid').value());
              }
              return console.log('Err downloading assets:', err);
            }
            
            win.webContents.send('progress-end');
            
            generatePlaylist(body, (err, playlist) => {
              if(err) return console.log('Err generating playlist:', err);
              callback(err, playlist);
            });
          });
        return;
      }
      callback();
    });
  },
  getAssetPath = (asset, callback) => {
    const
      assetDir = path.join('assets', asset.id);
    assetPath = path.join(assetDir, asset.filename);
    
    fs.ensureDir(path.join(__dirname, assetDir), err => {
      if(err) return callback(err);
      callback(null, assetPath);
    });
  },
  downloadAsset = (asset, callback) => {
    const
      record = db.get('assets.entries').find({id: asset.id}).value();
    if(record) {
      asset = _.extend(asset, {assetPath: record.assetPath});
      if(assetDownloaded(asset)) {
        return callback(null, asset);
      }
    }
    
    getAssetPath(asset, (err, assetPath) => {
      if(err) return callback(err);
      asset = _.extend(asset, {assetPath});
      if(assetDownloaded(asset)) {
        db.get('assets.entries').push(asset).write();
        return callback(null, asset);
      }
      
      downloadAssetFile(asset, err => {
        if(err) return callback(err);
        db.get('assets.entries').push(asset).write();
        callback(null, asset);
      });
    });
  },
  assetDownloaded = asset => {
    const
      assetPath = asset.assetPath;
    
    try {
      const stat = fs.statSync(path.join(__dirname, assetPath));
      return stat.isFile() && stat.size >= asset.filesize - 102400;
    }
    catch(err) {
    }
    return false;
  },
  downloadAssetFile = (asset, callback) => {
    const
      assetDir = path.join(__dirname, 'assets', asset.id),
      assetUrl = asset.url;
    
    if(downloadingAssets[asset.id] === true) {
      console.log('Asset', asset.id, 'is in download queue:', assetUrl);
      return callback();
    }
    
    downloadingAssets[asset.id] = true;
    download(assetUrl, {directory: assetDir, filename: asset.filename}, err => {
      if(err) {
        downloadingAssets[asset.id] = false;
        try {
          fs.removeSync(assetDir);
        }
        catch(err) {
          console.error('Error deleting unfinished asset:', path.join(assetDir));
        }
        return callback(err);
      }
      downloadingAssets[asset.id] = false;
      callback();
    });
  };


process.on('uncaughtException', err => {
  console.error('uncaughtException:', err.message);
  console.error(err.stack);
  process.exit(-1);
});

// DEBUGGING MEMORY USAGE
setInterval(() => {
  const used = process.memoryUsage();
  console.log(used);
  if (used.rss > 300 * 1024 * 1024) {
    console.log('Got higher memory allocation. RSS:', Math.round(used.rss/1024/1024*100)/100+'MB');
    throw new Error('Got memory leak!');
  }
}, 10000);